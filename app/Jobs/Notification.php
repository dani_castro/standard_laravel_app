<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\OneSignal\OneSignal as OneSignalService;

class Notification implements ShouldQueue
{

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var array
	 */
	private $devices = [];

	/**
	 * Create a new job instance.
	 *
	 * @param array $userIds
	 * @param Model $model
	 */
	public function __construct(array $userIds, Model $model)
	{

		if ( count($userIds) ) {
			$userIds = User::whereIn('id', $userIds)->select('id')->pluck('id')->toArray();

			$this->devices = null;//TODO: Get onesignal devices ids
		}

		$this->model = $model;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if ( ! empty($this->devices) ) {

			$oneSignal = new OneSignalService;

			$oneSignal->sendToSpecificDevices($this->devices, [
				'en' => null, //TODO: Set message
			], [//TODO: Payload
				'key' => null, //TODO: Set key
				'value' => $this->model->id
			]);
		}
	}

	/**
	 * Get devices
	 *
	 * @return mixed
	 */
	public function getDevices()
	{
		return $this->devices;
	}
}
