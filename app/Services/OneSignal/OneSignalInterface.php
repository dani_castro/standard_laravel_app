<?php

namespace App\Services\OneSignal;

interface OneSignalInterface
{

    /**
     * Sends to specific devices
     *
     * @param array $identifiers
     * @param $contents
     * @param $data
     * @return mixed
     */
    public function sendToSpecificDevices(array $identifiers, $contents, $data = []);
}