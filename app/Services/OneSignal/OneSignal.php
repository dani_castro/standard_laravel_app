<?php

namespace App\Services\OneSignal;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class OneSignal implements OneSignalInterface
{
	private $secret;
	private $appId;

	/**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    private $urlCreateNotification = 'https://onesignal.com/api/v1/notifications';

    /**
     * OneSignal constructor.
     */
    public function __construct()
    {
        $this->client = new Client;
        $this->appId = config('services.one-signal.id');
        $this->secret = config('services.one-signal.secret');
    }

    /**
     * Sends notifications based on specific devices
     *
     * @param array $identifiers
     * @param $contents
     * @param $data
     * @return bool
     */
    public function sendToSpecificDevices(array $identifiers, $contents, $data = [])
    {
        $requestBody = [
            'app_id'             => $this->appId,
            'include_player_ids' => $identifiers,
            'data'               => $data,
            'contents'           => $contents
        ];

        $response = $this->client->post($this->urlCreateNotification, [
            'headers' => [
                'Content-Type' . 'application/json; charset=utf-8',
                'Authorization: Basic ' . $this->secret
            ],
            'json'    => $requestBody
        ]);

        return $this->processResponse($response);
    }

    /**
     * Process the response from OneSignal
     *
     * @param ResponseInterface $response
     * @return bool
     */
    private function processResponse(ResponseInterface $response)
    {
        $contents = json_decode($response->getBody()->getContents());

        if ( isset($contents->errors) ) {

            if ( isset($contents->errors->invalid_players_ids) ) {
                //TODO: Something
            }

            return false;
        }

        return true;
    }

}