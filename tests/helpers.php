<?php

/**
 * @param $class
 * @param array $attributes
 * @param null $times
 *
 * @param array $states
 *
 * @return mixed
 */
function create( $class, $attributes = [], $times = null, $states = [] )
{
	return factory( $class, $times )->states($states)->create( $attributes );
}

function make( $class, $attributes = [], $times = null, $states = [] )
{
	return factory( $class, $times )->states($states)->make( $attributes );
}

function raw( $class, $attributes = [], $times = null, $states = [] )
{
	return factory( $class, $times )->states($states)->raw( $attributes );
}